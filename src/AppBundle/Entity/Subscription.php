<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subscription
 *
 * @ORM\Table(name="subscription")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubscriptionRepository")
 */
class Subscription
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="subscription_shipping_address_id", type="integer", nullable=true)
     */
    private $subscriptionShippingAddressId;

    /**
     * @var int
     *
     * @ORM\Column(name="subscription_billing_address_id", type="integer", nullable=true)
     */
    private $subscriptionBillingAddressId;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=16)
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="subscription_pack_id", type="integer")
     */
    private $subscriptionPackId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="started_at", type="datetime", nullable=true)
     */
    private $startedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Subscription
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set subscriptionShippingAddressId
     *
     * @param integer $subscriptionShippingAddressId
     *
     * @return Subscription
     */
    public function setSubscriptionShippingAddressId($subscriptionShippingAddressId)
    {
        $this->subscriptionShippingAddressId = $subscriptionShippingAddressId;

        return $this;
    }

    /**
     * Get subscriptionShippingAddressId
     *
     * @return int
     */
    public function getSubscriptionShippingAddressId()
    {
        return $this->subscriptionShippingAddressId;
    }

    /**
     * Set subscriptionBillingAddressId
     *
     * @param integer $subscriptionBillingAddressId
     *
     * @return Subscription
     */
    public function setSubscriptionBillingAddressId($subscriptionBillingAddressId)
    {
        $this->subscriptionBillingAddressId = $subscriptionBillingAddressId;

        return $this;
    }

    /**
     * Get subscriptionBillingAddressId
     *
     * @return int
     */
    public function getSubscriptionBillingAddressId()
    {
        return $this->subscriptionBillingAddressId;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Subscription
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set subscriptionPackId
     *
     * @param integer $subscriptionPackId
     *
     * @return Subscription
     */
    public function setSubscriptionPackId($subscriptionPackId)
    {
        $this->subscriptionPackId = $subscriptionPackId;

        return $this;
    }

    /**
     * Get subscriptionPackId
     *
     * @return int
     */
    public function getSubscriptionPackId()
    {
        return $this->subscriptionPackId;
    }

    /**
     * Set startedAt
     *
     * @param \DateTime $startedAt
     *
     * @return Subscription
     */
    public function setStartedAt($startedAt)
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    /**
     * Get startedAt
     *
     * @return \DateTime
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Subscription
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Subscription
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}

