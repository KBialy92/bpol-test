<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SubscriptionPayment
 *
 * @ORM\Table(name="subscription_payment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubscriptionPaymentRepository")
 */
class SubscriptionPayment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="subscription_id", type="integer", nullable=true)
     */
    private $subscriptionId;

    /**
     * @var int
     *
     * @ORM\Column(name="charged_amount", type="integer")
     */
    private $chargedAmount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subscriptionId
     *
     * @param integer $subscriptionId
     *
     * @return SubscriptionPayment
     */
    public function setSubscriptionId($subscriptionId)
    {
        $this->subscriptionId = $subscriptionId;

        return $this;
    }

    /**
     * Get subscriptionId
     *
     * @return int
     */
    public function getSubscriptionId()
    {
        return $this->subscriptionId;
    }

    /**
     * Set chargedAmount
     *
     * @param integer $chargedAmount
     *
     * @return SubscriptionPayment
     */
    public function setChargedAmount($chargedAmount)
    {
        $this->chargedAmount = $chargedAmount;

        return $this;
    }

    /**
     * Get chargedAmount
     *
     * @return int
     */
    public function getChargedAmount()
    {
        return $this->chargedAmount;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return SubscriptionPayment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return SubscriptionPayment
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return SubscriptionPayment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    private function isVisa(int $cardNumber) : bool
    {
        if (strlen($cardNumber) == 16 && substr($cardNumber, 0, 1) == 4) {
            return true;
        }
        return false;
    }

    private function isMasterCard(int $cardNumber) : bool
    {
        if (strlen($cardNumber) == 16 && in_array(substr($cardNumber, 0, 2), [51, 52, 53, 54, 55])) {
            return true;
        }
        return false;
    }

    private function isAmericanExpress(int $cardNumber) : bool
    {
        if (strlen($cardNumber) == 15 && in_array(substr($cardNumber, 0, 2), [34, 37])) {
            return true;
        }
        return false;
    }

    public function validateCreditCard(int $cardNumber) : mixed
    {
        switch ($cardNumber) {
            case $this->isVisa($cardNumber):
                $cardType = 'VI';
                break;
            case $this->isAmericanExpress($cardNumber):
                $cardType = 'AE';
                break;
            case $this->isMasterCard($cardNumber):
                $cardType = 'MC';
                break;
            default:
                return false;
        }

        return $cardType;
    }

}

