<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Subscriber;
use AppBundle\Entity\Subscription;
use AppBundle\Entity\SubscriptionPayment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class SubscriptionController extends Controller
{
    /**
     * @Route("/subscribe")
     */
    public function subscribeAction(Request $request)
    {

        // Mock usera na wyłącznie na potrzeby zadania testowego
        $subscriber = new Subscriber();
        $subscriptionPayment = new SubscriptionPayment();

        $form = $this->createFormBuilder($subscriber)
            ->add('card_number', IntegerType::class)
            ->add('card_type', ChoiceType::class, array(
                'choices' => array(
                    'MasterCard'        => 'MS',
                    'Visa'              => 'VI',
                    'AmericanExpress'   => 'AE'
                )
            ))
            ->add('save', SubmitType::class, array('label' => 'Subscribe'))
            ->getForm();

        if ($form->isSubmitted() && $form->isValid()) {

            $cvv        = $request->get('cvv'); // not used
            $cardNumber = $request->get('card_number');
            $cardType   = $request->get('card_type');

            if ($subscriptionPayment->validateCreditCard($cardNumber) == $cardType) {
                $subscription = $this->getDoctrine()
                    ->getRepository('AppBundle:Subscription')
                    ->find(1);

                $subscription->setStatus('active');
                $subscription->flush();

                $message = new \Swift_Message('Subscription')
                    ->setFrom('foo@bar.com')
                    ->setTo('user@mail.com')
                    ->setBody(
                        $this->renderView(
                            '/Emails/SubscriptionTest.html.twig',
                            array('name' => 'some user name')
                        ),
                        'text/html'
                    );
                $this->get('mailer')->send($message);
            }
        }

        return $this->render('AppBundle:Subscription:subscribe.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
