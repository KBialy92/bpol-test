<?php

namespace AppBundle\Command;

use AppBundle\Entity\Subscription;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class DeactivateUnpaidSubsCommand extends Command
{
    protected function configure()
    {
        $this->setName('subscription:deactivate-unpaid');
        $this->setDescription('Deactivates unpaid subscription');
        $this->setHelp('This command will deactivate subscriptions that have not been paid within 7 days');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repository = $this->getContainer()->get('AppBundle:SubscriptionPayment');

        $query= $repository->createQueryBuilder('s')
            ->where('DATE(s.date) + INTERVAL 7 DAY  > CURDATE()')
            ->getQuery();

        $subscriptionPayments = $query->getResults();

        $subscriptionRepo = $this->getContainer()->get('AppBundle:Subscription');
        foreach ($subscriptionPayments as $payment) {
            $subsription = $subscriptionRepo->find($payment->id);
            $subsription->setStatus('deactivated');
            $subsription->flush();
        }
    }
}